java -version 2>&1 | tee postak.log
echo "====================" >> postak.log
uname -a 2>&1 >> postak.log
echo "====================" >> postak.log
echo "Pokus o spuštění hry" >> postak.log
echo "--------------------" >> postak.log
export LD_LIBRARY_PATH="/usr/lib/jvm/java-7-oracle/jre/lib/i386/"
export LD_LIBRARY_PATH="/usr/lib/jvm/java-7-oracle/jre/lib/amd64/"
export LD_LIBRARY_PATH="./"
java -jar Postak.jar 2>&1 >> postak.log
echo "====================" 2>&1 >> postak.log
echo "Obecné informace:" 2>&1 >> postak.log
echo "--------------------" 2>&1 >> postak.log
echo "Vygenerováno:" $(date +%Y%m%d_%H:%M) 2>&1 >> postak.log
echo "Verze autotestu: 0.2" 2>&1 >> postak.log
echo "Tento soubor přiložte prosím k hlášení o chybě na www.postak.masteride.cz. Díky! :)" 2>&1 >> postak.log
echo "====================" 2>&1 >> postak.log
echo "Autor:" 2>&1 >> postak.log
echo "--------------------" 2>&1 >> postak.log
echo "(cc) $(date +%Y) Tomáš Moravec - www.tmoravec.cz" 2>&1 >> postak.log
echo "www.postak.masteride.cz" 2>&1 >> postak.log
echo "====================" 2>&1 >> postak.log
cat /proc/cpuinfo 2>&1 >> postak.log
mv postak.log postak_$(date +%Y%m%d_%H%M).log