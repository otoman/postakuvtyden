C:\Windows\System32\Java.exe -version 2> postak.log
echo ==================== >> postak.log
ver >> postak.log
echo Procesor: %PROCESSOR_ARCHITECTURE% >> postak.log
echo System: >> postak.log
 if not exist "%systemdrive%\Program Files (x86)" (
echo 32-bit >> postak.log
) else (
echo 64-bit >> postak.log
)
echo ==================== >> postak.log
echo Pokus o spusteni hry: >> postak.log
echo -------------------- >> postak.log
C:\Windows\System32\Java.exe -jar Postak.jar >> postak.log
echo ==================== >> postak.log
echo Obecne informace: >> postak.log
echo -------------------- >> postak.log
echo Vygenerovano: %date% v %time% >> postak.log
echo Verze autotestu: 0.2 >> postak.log
echo Tento soubor prilozte prosim k hlaseni o chybe na www.postak.masteride.cz. Diky! :)>> postak.log
echo ==================== >> postak.log
echo Autor: >> postak.log
echo -------------------- >> postak.log
echo (cc) 2013 Tomas Moravec - www.tmoravec.cz >> postak.log
echo www.postak.masteride.cz >> postak.log
rename postak.log postak_%date:~-4,4%%date:~-10,2%%date:~-7,2%_%time:~0,2%%time:~3,2%%time:~6,2%.log