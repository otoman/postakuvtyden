package core;

import com.threed.jpct.*;
import com.threed.jpct.util.KeyMapper;
import com.threed.jpct.util.KeyState;
import core.objects.Postman;
import core.utils.Blity;
import core.states.Menu;
import core.states.States;
import core.states.Game;
import core.states.End;
import core.states.Score;
import core.utils.MouseMapper;
import core.utils.MusicManager;
import core.utils.ScoreManager;
import core.utils.SoundManager;
import java.awt.Color;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import org.lwjgl.input.Mouse;

/**
 *
 * @author tomas
 */
public class Start {

    private static World svet = null;
    private static FrameBuffer frameBuffer = null;
    private KeyMapper keyMapper = null;
    private MouseMapper mouseMapper = null;
    private static boolean veHre = false;
    private static byte stav = 0;
    private static MusicManager hudebniManazer = null;
    private final SoundManager soundManager;
    public static ScoreManager scoreManager = null;
    private static boolean skore = false;
    private boolean doLoop = true;
    public static int width = 0;
    public static int height = 0;
    public static boolean restart;
    private Blity blity;
    public static int prepnuti;
    private static boolean tichoHudba;
    private boolean tichoZvuky;
    private static boolean povolitMlhu;
    private Scanner sc;
    public static boolean pauza = false;
    public static byte inkrementZvyrazneniNabidek = 0;
    public static byte inkrementZvyrazneniMenu = 0;
    private boolean testPrepnuti;

    public Start() {
        Config.glWindowName = "Pošťákův týden";
        Config.glColorDepth = 24;
        Config.glFullscreen = true;
        Config.glVSync = true;

        init();
        try {
            sc = new Scanner(new File("settings.dat"));
            tichoHudba = Boolean.valueOf(sc.nextLine());
            tichoZvuky = Boolean.valueOf(sc.nextLine());
            povolitMlhu = Boolean.valueOf(sc.nextLine());
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
        }

        hudebniManazer = new MusicManager(tichoHudba);
        soundManager = new SoundManager(tichoZvuky);
        scoreManager = new ScoreManager();
        scoreManager.nacti();
    }

    private void init() {
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        width = gd.getDisplayMode().getWidth();
        height = gd.getDisplayMode().getHeight();
        frameBuffer = new FrameBuffer(width, height, FrameBuffer.SAMPLINGMODE_NORMAL);
        frameBuffer.disableRenderer(IRenderer.RENDERER_SOFTWARE);
        frameBuffer.enableRenderer(IRenderer.RENDERER_OPENGL, IRenderer.MODE_OPENGL);

        //svet
        svet = new Menu();

        //ovladani
        mouseMapper = new MouseMapper(frameBuffer);
        mouseMapper.hide();
        keyMapper = new KeyMapper();

        restart = false;

        blity = new Blity();
        prepnuti = 1;

    }

    private void loop() throws InterruptedException {
        while (doLoop) {

            hudebniManazer.ziskejStav();
            ovladani();
            pauzovani();

            //updaty aktuálního stavu
            ((States) svet).update();

            frameBuffer.clear(new Color(171, 233, 233));
            svet.renderScene(frameBuffer);
            svet.draw(frameBuffer);
            frameBuffer.update();
            blity.blit();
            frameBuffer.displayGLOnly();
            Thread.sleep(10);
        }
        dispose();
    }

    public static void main(String[] args) throws Exception {
        new Start().loop();
    }

    private void pauzovani() {
        if (!Mouse.isInsideWindow() && stav > 0 && stav < 6) {
            Blity.setNabidkaM(true);
            mouseMapper.show();
        }

        if (Postman.isZabit()) {
            pauza = true;
        } else if (Blity.isNabidkaM() || Blity.isNabidkaV()) {
            pauza = true;
        } else {
            pauza = false;
            mouseMapper.hide();
        }
    }

    private void ovladani() {
        KeyState ks = null;
        while ((ks = keyMapper.poll()) != KeyState.NONE) {

            //Vstup do hry z příběhu
            if (ks.getKeyCode() == KeyEvent.VK_ENTER && stav == 0 && !veHre && Menu.getI() == 2 && prepnuti == 2) {
                if (ks.getState()) {
                    System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                    jitDoHry();
                    prepnuti = 1;
                }
            }
            
            //Menu
            if (ks.getKeyCode() == KeyEvent.VK_DOWN && stav == 0 && Menu.getI() == 1 && !Blity.isNabidkaM() && !Blity.isNabidkaV() && inkrementZvyrazneniMenu < 3) {
                if (ks.getState()) {
                    inkrementZvyrazneniMenu++;
                }
            }

            if (ks.getKeyCode() == KeyEvent.VK_UP && stav == 0 && Menu.getI() == 1 && !Blity.isNabidkaM() && !Blity.isNabidkaV() && inkrementZvyrazneniMenu > 0) {
                if (ks.getState()) {
                    inkrementZvyrazneniMenu--;
                }
            }

            if (ks.getKeyCode() == KeyEvent.VK_ENTER && stav == 0 && Menu.getI() == 1 && !Blity.isNabidkaM() && !Blity.isNabidkaV() && prepnuti == 1) {
                switch (inkrementZvyrazneniMenu) {
                    case 1:
                        if (ks.getState()) {
                            System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                            System.out.println("Ovládání");
                            Menu.setI(3);
                            testPrepnuti = ks.getState();
                            if (testPrepnuti) {
                                prepnuti++;
                            }
                        }
                        break;
                    case 2:
                        if (ks.getState()) {
                            System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                            skore = true;
                            System.out.println("Skóre");
                            svet.dispose();
                            svet = new Score();
                            testPrepnuti = ks.getState();
                            if (testPrepnuti) {
                                prepnuti++;
                            }
                        }
                        break;
                    case 3:
                        if (ks.getState()) {
                            System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                            dispose();
                        }
                        break;
                    default:
                        if (ks.getState()) {
                            System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                            System.out.println("Příběh");
                            Menu.setI(2);
                            testPrepnuti = ks.getState();
                            if (testPrepnuti) {
                                prepnuti++;
                            }
                        }
                        break;
                }
            }

            //Dialog vypnutí
            if (ks.getKeyCode() == KeyEvent.VK_ESCAPE && stav == 0) {
                System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                if (ks.getState()) {
                    inkrementZvyrazneniNabidek = 0;
                    Blity.setNabidkaV(!Blity.isNabidkaV());
                    nabidkaVypnuti();
                }
            }

            if (ks.getKeyCode() == KeyEvent.VK_RIGHT && stav == 0 && Blity.isNabidkaV() && inkrementZvyrazneniNabidek == 0) {
                inkrementZvyrazneniNabidek++;
            }

            if (ks.getKeyCode() == KeyEvent.VK_LEFT && stav == 0 && Blity.isNabidkaV() && inkrementZvyrazneniNabidek == 1) {
                inkrementZvyrazneniNabidek--;
            }

            if (ks.getKeyCode() == KeyEvent.VK_ENTER && stav == 0 && Blity.isNabidkaV()) {
                if (inkrementZvyrazneniNabidek == 0) {
                    if (ks.getState()) {
                        System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                        dispose();
                    }
                } else {
                    if (ks.getState()) {
                        Blity.setNabidkaV(!Blity.isNabidkaV());
                    }
                }
            }

            //Dialog menu
            if (ks.getKeyCode() == KeyEvent.VK_ESCAPE && (veHre)) {
                System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                if (ks.getState()) {
                    inkrementZvyrazneniNabidek = 0;
                    Blity.setNabidkaM(!Blity.isNabidkaM());
                    nabidkaMenu();
                }
            }

            if (ks.getKeyCode() == KeyEvent.VK_DOWN && stav > 0 && stav < 6 && Blity.isNabidkaM() && inkrementZvyrazneniNabidek < 3) {
                if (ks.getState()) {
                    inkrementZvyrazneniNabidek++;
                }
            }

            if (ks.getKeyCode() == KeyEvent.VK_UP && stav > 0 && stav < 6 && Blity.isNabidkaM() && inkrementZvyrazneniNabidek > 0) {
                if (ks.getState()) {
                    inkrementZvyrazneniNabidek--;
                }
            }

            if (ks.getKeyCode() == KeyEvent.VK_ENTER && stav > 0 && stav < 6 && Blity.isNabidkaM()) {
                switch (inkrementZvyrazneniNabidek) {
                    case 1:
                        System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                        restart = ks.getState();
                        Blity.setNabidkaM(false);
                        break;
                    case 2:
                        if (ks.getState()) {
                            System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                            Blity.setNabidkaM(false);
                            menu();
                        }
                        break;
                    case 3:
                        if (ks.getState()) {
                            System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                            Blity.setNabidkaM(false);
                            dispose();
                        }
                        break;
                    default:
                        System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                        Blity.setNabidkaM(!Blity.isNabidkaM());
                        nabidkaMenu();
                        break;
                }
            }

            //Zpět do menu
            if (ks.getKeyCode() == KeyEvent.VK_ESCAPE && (skore || (stav == 0 && Menu.getI() != 1)) && !veHre) {
                if (ks.getState()) {
                    System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                    skore = false;
                    Blity.setNabidkaV(false);
                    menu();
                    prepnuti--;
                }
            }

            //Ovládání kamery
            if (ks.getKeyCode() == KeyEvent.VK_B && (veHre)) {
                System.out.println("Akce klávesy (rozepnutí/sepnutí) " + KeyEvent.getKeyText(ks.getKeyCode()));
                Game.setPriblizit(ks.getState());
            }

            if (ks.getKeyCode() == KeyEvent.VK_V && (veHre)) {
                System.out.println("Akce klávesy (rozepnutí/sepnutí) " + KeyEvent.getKeyText(ks.getKeyCode()));
                Game.setOddalit(ks.getState());
            }

            if (ks.getKeyCode() == KeyEvent.VK_C && (veHre)) {
                System.out.println("Akce klávesy (rozepnutí/sepnutí) " + KeyEvent.getKeyText(ks.getKeyCode()));
                Game.setVychoziPozice(ks.getState());
            }
            //

            //Ovládání pošťáka
            if (ks.getKeyCode() == KeyEvent.VK_E && stav > 0 && stav < 6) {
                Postman.setBeh(ks.getState());
            }

            if (ks.getKeyCode() == KeyEvent.VK_W && stav > 0 && stav < 6) {
                Postman.setDopredu(ks.getState());
            }

            if (ks.getKeyCode() == KeyEvent.VK_S && stav > 0 && stav < 6) {
                Postman.setDozadu(ks.getState());
            }

            if (ks.getKeyCode() == KeyEvent.VK_A && stav > 0 && stav < 6) {
                Postman.setDoleva(ks.getState());
            }

            if (ks.getKeyCode() == KeyEvent.VK_D && stav > 0 && stav < 6) {
                Postman.setDoprava(ks.getState());
            }

            if (ks.getKeyCode() == KeyEvent.VK_SPACE && stav > 0 && stav < 6) {
                Postman.setSkok(ks.getState());
            }
            //

            //Tajné přepínání kol
            if (ks.getKeyCode() == KeyEvent.VK_O && stav > 0 && stav < 6) {
                System.out.println("O => Přepnutí do dalšího kola");
                Game.setDalsiKolo(ks.getState());
            }

            //Restart
            if (ks.getKeyCode() == KeyEvent.VK_SPACE && stav > 0 && stav < 6 && Postman.isZabit()) {
                if (ks.getState()) {
                    System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                    restart = true;
                }
            }

            //Počet FPS
            if (ks.getKeyCode() == KeyEvent.VK_F11) {
                if (ks.getState()) {
                    if (ks.getState()) {
                        System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                        Blity.setBlitFPS(!Blity.isBlitFPS());
                    }
                }
            }

            if (ks.getKeyCode() == KeyEvent.VK_ENTER && stav == 6) {
                if (ks.getState()) {
                    System.out.println("Stisknuta klávesa " + KeyEvent.getKeyText(ks.getKeyCode()));
                    testPrepnuti = ks.getState();
                    if (testPrepnuti) {
                        prepnuti++;
                    }
                }
            }
        }

        Postman.setAnimace(2); //zastavení animace pošťáka

        if (org.lwjgl.opengl.Display.isCloseRequested()) {
            doLoop = false;
        }
    }

    private void nabidkaVypnuti() {
        if (Blity.isNabidkaM()) {
            Blity.setNabidkaM(false);
        }
    }

    private void nabidkaMenu() {
        if (Blity.isNabidkaV()) {
            Blity.setNabidkaV(false);
        }
    }

    public static void jitDoHry() {
        System.out.println("Jdu do hry");
        svet.dispose();
        svet = new Game();
        veHre = true;
    }

    public static void menu() {
        System.out.println("Jdu do menu");
        svet.dispose();
        svet = new Menu();
        veHre = false;
    }

    public static void konec() {
        System.out.println("Úspěšně dokončená hra");
        svet.dispose();
        svet = new End();
        veHre = false;
    }

    private void dispose() {
        if (!tichoHudba) {
            hudebniManazer.stop(); //zajištění aby hudba umřela dříve než obraz
        }
        if (!tichoZvuky) {
            soundManager.vycisti();
        }
        svet.removeAll();
        frameBuffer.dispose();
        System.exit(0);
    }

//    Stavy
//    0 menu
//    1 - 5 levely
//    6 konec
    public static byte getStav() {
        return stav;
    }

    public static void setStav(byte stav) {
        Start.stav = stav;
    }

    public static FrameBuffer getFrameBuffer() {
        return frameBuffer;
    }

    public static boolean isTichoHudba() {
        return tichoHudba;
    }

    public static boolean isScore() {
        return skore;
    }

    public static boolean isPovolitMlhu() {
        return povolitMlhu;
    }

    public static World getSvet() {
        return svet;
    }

}
