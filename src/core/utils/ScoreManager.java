/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.utils;

import core.states.Game;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tomas
 */
public class ScoreManager {

    private static String[] jmeno;
    private static int[] skore;

    private Scanner sc;
    private final int pocetMist = 3;
    private BufferedWriter bw;

    public ScoreManager() {
        jmeno = new String[3];
        skore = new int[3];
    }

    public void nacti() {
        System.out.println("-------Načtení výsledků ze souboru-------");
        try {
            sc = new Scanner(new File("score.dat"));
            String radkyNaSloupce;
            for (int i = 0; i < pocetMist; i++) {
                radkyNaSloupce = sc.nextLine();
                Scanner sc2 = new Scanner(radkyNaSloupce);
                jmeno[i] = sc2.next();
                skore[i] = sc2.nextInt();
                System.out.println(jmeno[i] + " " + skore[i]);
            }
            System.out.println("-------Načtení výsledků dokončeno-------");

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ScoreManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void zapis() {
        try {
            sc = new Scanner(new File("playerName.dat"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ScoreManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String jmenoHrace = sc.nextLine();

        if (Game.pocetBodu >= skore[0]) {
            for (int i = pocetMist - 1; i > 0; i--) {
                jmeno[i] = jmeno[i - 1];
                skore[i] = skore[i - 1];
            }
            jmeno[0] = jmenoHrace;
            skore[0] = Game.pocetBodu;
        }

        if (Game.pocetBodu >= skore[1] && Game.pocetBodu < skore[0]) {
            jmeno[2] = jmeno[1];
            skore[2] = skore[1];

            jmeno[1] = jmenoHrace;
            skore[1] = Game.pocetBodu;
        }

        if (Game.pocetBodu >= skore[2] && Game.pocetBodu < skore[1]) {
            jmeno[2] = jmenoHrace;
            skore[2] = Game.pocetBodu;
        }
        System.out.println("Pořadí po zápisu:");
        for (int i = 0; i < pocetMist; i++) {
            System.out.println(jmeno[i] + " " + skore[i]);
        }
        try {
            bw = new BufferedWriter(new FileWriter(new File("score.dat")));
            for (int i = 0; i < pocetMist; i++) {

                bw.write(jmeno[i] + " " + String.valueOf(skore[i]));
                bw.newLine();
            }
            bw.close();
            System.out.println("Výsledky úspěšně zapsány");
        } catch (IOException ex) {
            Logger.getLogger(ScoreManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static String getJmeno(int i) {
        return jmeno[i];
    }

    public static int getSkore(int i) {
        return skore[i];
    }

}
