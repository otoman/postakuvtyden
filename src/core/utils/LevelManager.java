/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core.utils;

import com.threed.jpct.Object3D;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tomas
 */
public class LevelManager {

    public final HashMap<String, Object3D> poziracBalikovyPole;
    public final HashMap<String, Object3D> poziracDopisovyPole;
    public final HashMap<String, Object3D> bednaPole;
    public final HashMap<String, Object3D> cihlaPole;
    public final HashMap<String, Object3D> dopisPole;
    public final HashMap<String, Object3D> mincePole;
    public final HashMap<String, Object3D> cilPole;
    private final String adresaLevelu = "assets/levels/level_";
    private final String adresaObjektu = "assets/objects/";
    private Scanner dalsiRezani;
    public Object3D mapa;
    private Object3D objekt;
    public byte pocetPD;
    public byte pocetPB;
    public int pocetB;
    public int pocetC;
    public byte pocetD;
    public byte pocetM;
    public byte pocetCilu;
    public byte cisloKola;
    private String jmeno;
    private int aktualniPocet = 0;

    public LevelManager(byte cisloKola) {
        this.poziracDopisovyPole = new HashMap<>();
        this.poziracBalikovyPole = new HashMap<>();
        this.bednaPole = new HashMap<>();
        this.cihlaPole = new HashMap<>();
        this.dopisPole = new HashMap<>();
        this.mincePole = new HashMap<>();
        this.cilPole = new HashMap<>();
        System.out.println("...............: Startuji level manažera :..............."); 
        nactiMapu(cisloKola);
        nactiTextury();
        nactiObjekty(cisloKola);
    }

    private void nactiMapu(byte cisloKola) {
        mapa = new Object3D(Load.loadModel("assets/maps/level_" + cisloKola + ".3ds", 65));
        mapa.translate(0, -8.7f, 0);
        TextureManager.getInstance().addTexture("map" + cisloKola, new Texture("assets/maps/level_" + cisloKola + ".png"));
    }

    private void nactiObjekty(byte cisloKola) {
        try {
            Scanner kolaNaRadky = new Scanner(new File(adresaLevelu + cisloKola + ".dat"));
            String retezec;
            for (int i = 0; kolaNaRadky.hasNextLine(); i++) {
                if (i == 0) {
                    System.out.println("");
                    System.out.println("--------------Načítám požírače dopisové--------------");
                    System.out.println("Řádek číslo: " + i);
                    retezec = kolaNaRadky.nextLine();
                    dalsiRezani = new Scanner(retezec);
                    while (dalsiRezani.hasNext()) {
                        jmeno = dalsiRezani.next();
                        pocetPD = dalsiRezani.nextByte();
                    }
                    System.out.println("Do hry má být načteno: " + pocetPD + "x " + jmeno);
                    aktualniPocet = 0;
                }
                if (i >= 1 && i < (1 + pocetPD)) {
                    System.out.println("Řádek číslo: " + i);
                    retezec = kolaNaRadky.nextLine();
                    dalsiRezani = new Scanner(retezec);
                    float[] param = new float[7]; // posun x, posun y, posun z, rotace x, rotace y, rotace z, scale
                    for (int x = 0; dalsiRezani.hasNextFloat(); x++) {
                        param[x] = dalsiRezani.nextFloat();
                    }
                    param[6] = 0.15f;
                    vytvorObjekt(jmeno, poziracDopisovyPole, param);
                    aktualniPocet++;
                }

                if (i == (1 + pocetPD)) {
                    System.out.println("");
                    System.out.println("--------------Načítám požírače balíkové--------------");
                    System.out.println("Řádek číslo: " + i);
                    retezec = kolaNaRadky.nextLine();
                    dalsiRezani = new Scanner(retezec);
                    while (dalsiRezani.hasNext()) {
                        jmeno = dalsiRezani.next();
                        pocetPB = dalsiRezani.nextByte();
                    }
                    System.out.println("Do hry má být načteno: " + pocetPB + "x " + jmeno);
                    aktualniPocet = 0;
                }
                if (i >= 2 + pocetPD && i < 2 + pocetPB + pocetPD) {
                    System.out.println("Řádek číslo: " + i);
                    retezec = kolaNaRadky.nextLine();
                    dalsiRezani = new Scanner(retezec);
                    float[] param = new float[7]; // posun x, posun y, posun z, rotace x, rotace y, rotace z, scale
                    for (int x = 0; dalsiRezani.hasNextFloat(); x++) {
                        param[x] = dalsiRezani.nextFloat();
                    }
                    param[6] = 0.4f;
                    vytvorObjekt(jmeno, poziracBalikovyPole, param);
                    aktualniPocet++;
                }

                if (i == 2 + pocetPB + pocetPD) {
                    System.out.println("");
                    System.out.println("--------------Načítám bedny--------------");
                    System.out.println("Řádek číslo: " + i);
                    retezec = kolaNaRadky.nextLine();
                    dalsiRezani = new Scanner(retezec);
                    while (dalsiRezani.hasNext()) {
                        jmeno = dalsiRezani.next();
                        pocetB = dalsiRezani.nextInt();
                    }
                    System.out.println("Do hry má být načteno: " + pocetB + "x " + jmeno);
                    aktualniPocet = 0;
                }
                if (i >= 3 + pocetPD + pocetPB && i < 3 + pocetPD + pocetPB + pocetB) {
                    System.out.println("Řádek číslo: " + i);
                    retezec = kolaNaRadky.nextLine();
                    dalsiRezani = new Scanner(retezec);
                    float[] param = new float[7]; // posun x, posun y, posun z, rotace x, rotace y, rotace z, scale
                    for (int x = 0; dalsiRezani.hasNextFloat(); x++) {
                        param[x] = dalsiRezani.nextFloat();
                    }
                    param[6] = 1f;
                    vytvorObjekt(jmeno, bednaPole, param);
                    aktualniPocet++;
                }

                if (i == 3 + pocetPD + pocetPB + pocetB) {
                    System.out.println("");
                    System.out.println("--------------Načítám cihly--------------");
                    System.out.println("Řádek číslo: " + i);
                    retezec = kolaNaRadky.nextLine();
                    dalsiRezani = new Scanner(retezec);
                    while (dalsiRezani.hasNext()) {
                        jmeno = dalsiRezani.next();
                        pocetC = dalsiRezani.nextInt();
                    }
                    System.out.println("Do hry má být načteno: " + pocetC + "x " + jmeno);
                    aktualniPocet = 0;
                }
                if (i >= 4 + pocetPD + pocetPB + pocetB && i < 4 + pocetPD + pocetPB + pocetB + pocetC) {
                    System.out.println("Řádek číslo: " + i);
                    retezec = kolaNaRadky.nextLine();
                    dalsiRezani = new Scanner(retezec);
                    float[] param = new float[7]; // posun x, posun y, posun z, rotace x, rotace y, rotace z, scale
                    for (int x = 0; dalsiRezani.hasNextFloat(); x++) {
                        param[x] = dalsiRezani.nextFloat();
                    }
                    param[6] = 1f;
                    vytvorObjekt(jmeno, cihlaPole, param);
                    aktualniPocet++;
                }

                if (i == 4 + pocetPD + pocetPB + pocetB + pocetC) {
                    System.out.println("");
                    System.out.println("--------------Načítám dopisy--------------");
                    System.out.println("Řádek číslo: " + i);
                    retezec = kolaNaRadky.nextLine();
                    dalsiRezani = new Scanner(retezec);
                    while (dalsiRezani.hasNext()) {
                        jmeno = dalsiRezani.next();
                        pocetD = dalsiRezani.nextByte();
                    }
                    System.out.println("Do hry má být načteno: " + pocetD + "x " + jmeno);
                    aktualniPocet = 0;
                }
                if (i >= 5 + pocetPD + pocetPB + pocetB + pocetC && i < 5 + pocetPD + pocetPB + pocetB + pocetC + pocetD) {
                    System.out.println("Řádek číslo: " + i);
                    retezec = kolaNaRadky.nextLine();
                    dalsiRezani = new Scanner(retezec);
                    float[] param = new float[7]; // posun x, posun y, posun z, rotace x, rotace y, rotace z, scale
                    for (int x = 0; dalsiRezani.hasNextFloat(); x++) {
                        param[x] = dalsiRezani.nextFloat();
                    }
                    param[6] = 0.1f;
                    vytvorObjekt(jmeno, dopisPole, param);
                    aktualniPocet++;
                }

                if (i == 5 + pocetPD + pocetPB + pocetB + pocetC + pocetD) {
                    System.out.println("");
                    System.out.println("--------------Načítám mince--------------");
                    System.out.println("Řádek číslo: " + i);
                    retezec = kolaNaRadky.nextLine();
                    dalsiRezani = new Scanner(retezec);
                    while (dalsiRezani.hasNext()) {
                        jmeno = dalsiRezani.next();
                        pocetM = dalsiRezani.nextByte();
                    }
                    System.out.println("Do hry má být načteno: " + pocetM + "x " + jmeno);
                    aktualniPocet = 0;
                }
                if (i >= 6 + pocetPD + pocetPB + pocetB + pocetC + pocetD && i < 6 + pocetPD + pocetPB + pocetB + pocetC + pocetD + pocetM) {
                    System.out.println("Řádek číslo: " + i);
                    retezec = kolaNaRadky.nextLine();
                    dalsiRezani = new Scanner(retezec);
                    float[] param = new float[7]; // posun x, posun y, posun z, rotace x, rotace y, rotace z, scale
                    for (int x = 0; dalsiRezani.hasNextFloat(); x++) {
                        param[x] = dalsiRezani.nextFloat();
                    }
                    param[6] = 1f;
                    vytvorObjekt(jmeno, mincePole, param);
                    aktualniPocet++;
                }

                if (i == 6 + pocetPD + pocetPB + pocetB + pocetC + pocetD + pocetM) {
                    System.out.println("");
                    System.out.println("--------------Načítám cíl--------------");
                    System.out.println("Řádek číslo: " + i);
                    retezec = kolaNaRadky.nextLine();
                    dalsiRezani = new Scanner(retezec);
                    while (dalsiRezani.hasNext()) {
                        jmeno = dalsiRezani.next();
                        pocetCilu = dalsiRezani.nextByte();
                    }
                    System.out.println("Do hry má být načteno: " + pocetCilu + "x " + jmeno);
                    aktualniPocet = 0;
                }
                if (i >= 7 + pocetPD + pocetPB + pocetB + pocetC + pocetD + pocetM && i < 7 + pocetPD + pocetPB + pocetB + pocetC + pocetD + pocetM + pocetCilu) {
                    System.out.println("Řádek číslo: " + i);
                    retezec = kolaNaRadky.nextLine();
                    dalsiRezani = new Scanner(retezec);
                    float[] param = new float[7]; // posun x, posun y, posun z, rotace x, rotace y, rotace z, scale
                    for (int x = 0; dalsiRezani.hasNextFloat(); x++) {
                        param[x] = dalsiRezani.nextFloat();
                    }
                    param[6] = 20f;
                    vytvorObjekt(jmeno, cilPole, param);
                    aktualniPocet++;
                }

                if (pocetPD + pocetPB + pocetB + pocetC + pocetD + pocetM + pocetCilu == 0) {
                    System.out.println("");
                    System.out.println("=========CHYBA!=========");
                    System.out.println("Chyba při načítání objektů!");
                    System.out.println("KONEC");
                    System.exit(1);
                }
            }
            System.out.println("");
            System.out.println("--------------Konec LM--------------");
            this.cisloKola = cisloKola;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LevelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void nactiTextury() {
        TextureManager.getInstance().addTexture("poziracDopisovy", new Texture("assets/objects/poziracDopisovy.png"));
        TextureManager.getInstance().addTexture("poziracBalikovy", new Texture("assets/objects/poziracBalikovy.png"));
        TextureManager.getInstance().addTexture("bedna", new Texture("assets/objects/bedna.jpg"));
        TextureManager.getInstance().addTexture("cihla", new Texture("assets/objects/cihla.jpg"));
        TextureManager.getInstance().addTexture("dopis", new Texture("assets/objects/dopis.png"));
        TextureManager.getInstance().addTexture("mince", new Texture("assets/objects/mince.png"));
        TextureManager.getInstance().addTexture("cil", new Texture("assets/objects/cil.png"));
    }

    private void vytvorObjekt(String jmeno, HashMap<String, Object3D> jmenoPole, float[] param) {
        objekt = new Object3D(new Object3D(Load.loadModel(adresaObjektu + jmeno + ".3ds", param[6])));
        objekt.setTexture(jmeno);
        objekt.translate(param[0], param[1], param[2]);
        objekt.rotateX(param[3]);
        objekt.rotateY(param[4]);
        objekt.rotateZ(param[5]);
        jmenoPole.put(jmeno + aktualniPocet, objekt);
        System.out.println("Vytvořen objekt: " + jmeno + " " + aktualniPocet);
    }

    public void vycistiKolo() {
        try {
            System.out.println("Level manažer - čištění");
            mapa.clearObject();
            poziracDopisovyPole.clear();
            poziracBalikovyPole.clear();
            bednaPole.clear();
            cihlaPole.clear();
            mincePole.clear();
            dopisPole.clear();
            cilPole.clear();
            objekt.clearObject();
            TextureManager.getInstance().flush();
            System.out.println("Vyčištěno");
        } catch (Exception e) {
            System.out.println("Chyba: nepodařilo se vyčistit kolo " + cisloKola + ".");
        }
    }
}