/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.utils;

import com.threed.jpct.FrameBuffer;
import core.Start;
import static core.Start.getStav;
import core.objects.Postman;
import core.states.Game;
import core.states.Menu;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author tomas
 */
public class Blity {

    private String jmenoSkladby = null;
    private String jmenoInterpreta = null;
    private String jmenoKola = null;
    private final FrameBuffer frameBuffer = Start.getFrameBuffer();
    private BufferedImage cernePozadiImg = null;
    private BufferedImage listaImg = null;
    private BufferedImage dopisImg = null;
    private BufferedImage minceImg = null;
    private BufferedImage zlataImg = null;
    private BufferedImage stribrnaImg = null;
    private BufferedImage bronzovaImg = null;
    private BufferedImage pauzaImg = null;
    private int stribrnaInt = 0;
    private int zlataInt = 0;
    private int bronzovaInt = 0;
    private int horPozBod = 0;
    private int listaInt = 0;
    private int dopisInt = 0;
    private int minceInt = 0;
    private int cernePozadiInt = 0;
    private int pauzaInt = 0;
    private final TexturePack tp = new TexturePack();
    private final int width = Start.width;
    private final int height = Start.height;
    private static boolean nabidkaV = false;
    private static boolean nabidkaM = false;
    private static boolean blitFPS = false;
    private int vyslDelka;
    private long time = System.currentTimeMillis();
    private int lfps;
    private int fps;
    private GLFont text20;
    private GLFont text30;
    private GLFont text80;
    private GLFont text50;
    private BufferedImage listaMenuImg;
    private int listaMenuInt;

    public Blity() {
        init();
    }

    private void init() {
        try {
            listaImg = ImageIO.read(new File("assets/textures/lista.png"));
            dopisImg = ImageIO.read(new File("assets/textures/dopisIkona.png"));
            minceImg = ImageIO.read(new File("assets/textures/minceIkona.png"));
            pauzaImg = ImageIO.read(new File("assets/textures/pauzaIkona.png"));
            cernePozadiImg = ImageIO.read(new File("assets/textures/cerna.png"));
            zlataImg = ImageIO.read(new File("assets/textures/medals/gold.png"));
            stribrnaImg = ImageIO.read(new File("assets/textures/medals/silver.png"));
            bronzovaImg = ImageIO.read(new File("assets/textures/medals/bronze.png"));
            listaMenuImg = ImageIO.read(new File("assets/textures/listaMenu.png"));
            listaInt = tp.addImage(listaImg);
            dopisInt = tp.addImage(dopisImg);
            minceInt = tp.addImage(minceImg);
            cernePozadiInt = tp.addImage(cernePozadiImg);
            zlataInt = tp.addImage(zlataImg);
            stribrnaInt = tp.addImage(stribrnaImg);
            bronzovaInt = tp.addImage(bronzovaImg);
            pauzaInt = tp.addImage(pauzaImg);
            listaMenuInt = tp.addImage(listaMenuImg);
            tp.pack(true);

            Font font;
            try {
                font = Font.createFont(Font.TRUETYPE_FONT, new File("assets/fonts/Ubuntu-B.ttf"));
                font = font.deriveFont(Font.PLAIN, 20);
                text20 = GLFont.getGLFont(font);

                font = font.deriveFont(Font.PLAIN, 30);
                text30 = GLFont.getGLFont(font);

                font = font.deriveFont(Font.PLAIN, 50);
                text50 = GLFont.getGLFont(font);

                font = font.deriveFont(Font.PLAIN, 80);
                text80 = GLFont.getGLFont(font);

                GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
                ge.registerFont(font);
            } catch (FontFormatException ex) {
                Logger.getLogger(Blity.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void blit() {
        skore();
        lista();
        menu();
        nabidkaVypnuti();
        nabidkaMenu();
        zabitiPostaka();
        fpsCounter();
    }

    private void skore() {
        if (Start.isScore()) {
            int y = height / 2 + 50;
            int x = width / 2;
            tp.blit(frameBuffer, zlataInt, x - 500, y - 100, 250, 250, 255, false, null);
            text30.blitString(frameBuffer, ScoreManager.getJmeno(0), x - 375 - text30.getStringBounds(ScoreManager.getJmeno(0)).width / 2, y + 180, 255, Color.white);
            text30.blitString(frameBuffer, String.valueOf(ScoreManager.getSkore(0)), x - 375 - text30.getStringBounds("" + ScoreManager.getSkore(0)).width / 2, y + 220, 255, Color.white);

            tp.blit(frameBuffer, stribrnaInt, x - 125, y - 100, 250, 250, 255, false, null);
            text30.blitString(frameBuffer, ScoreManager.getJmeno(1), x - text30.getStringBounds(ScoreManager.getJmeno(1)).width / 2, y + 180, 255, Color.white);
            text30.blitString(frameBuffer, String.valueOf(ScoreManager.getSkore(1)), x - text30.getStringBounds("" + ScoreManager.getSkore(1)).width / 2, y + 220, 255, Color.white);

            tp.blit(frameBuffer, bronzovaInt, x + 250, y - 100, 250, 250, 255, false, null);
            text30.blitString(frameBuffer, ScoreManager.getJmeno(2), x + 375 - text30.getStringBounds(ScoreManager.getJmeno(2)).width / 2, y + 180, 255, Color.white);
            text30.blitString(frameBuffer, String.valueOf(ScoreManager.getSkore(2)), x + 375 - text30.getStringBounds("" + ScoreManager.getSkore(2)).width / 2, y + 220, 255, Color.white);
        }
    }

    private void lista() {
        if (getStav() > 0 && getStav() < 6) {
            tp.blit(frameBuffer, listaInt, 0, 0, width, 80, 10, false, null);
            tp.blit(frameBuffer, dopisInt, 20, 0, true);
            tp.blit(frameBuffer, minceInt, 100, 0, true);

            if (Start.pauza) {
                tp.blit(frameBuffer, pauzaInt, 170, 0, true);
                text20.blitString(frameBuffer, "PAUZA", 166, 73, 255, Color.ORANGE);
            }

            int pocetDopisu = Game.pocetDopisu;
            int celkemDopisu = Game.getDopisy().length;
            int pocetBodu = Game.pocetBodu;
            float procento = (float) pocetDopisu / ((float) celkemDopisu / 100);
            int x = 38;
            int y = 73;
            if (pocetDopisu == celkemDopisu) {
                text20.blitString(frameBuffer, "Najdi cíl", x - 24, y, 255, Color.black);
            } else {
                if (procento <= 35.0) {
                    text20.blitString(frameBuffer, pocetDopisu + "/" + celkemDopisu, x, y, 255, Color.red);
                } else if (procento <= 65.0) {
                    text20.blitString(frameBuffer, pocetDopisu + "/" + celkemDopisu, x, y, 255, Color.orange);
                } else if (procento <= 100.0) {
                    text20.blitString(frameBuffer, pocetDopisu + "/" + celkemDopisu, x, y, 255, Color.green);
                } else {
                    text20.blitString(frameBuffer, "CHYBA!", x - 25, y, 255, Color.magenta);
                }
            }

            int pocetCislicBodu = String.valueOf(pocetBodu).length();
            for (int i = 0; i < 6; i++) {
                if (pocetCislicBodu == i) {
                    horPozBod = (x + 88) - (5 * (i - 1));
                }
            }

            text20.blitString(frameBuffer, "" + pocetBodu, horPozBod, y, 255, Color.black);

            // center
            jmenoKola();
            text20.blitString(frameBuffer, "" + jmenoKola, (width / 2) - 40, y - 25, 255, Color.white);
        }
        if (!Start.isTichoHudba()) {
            int x = width - 20;
            int y = 29;
            priradNazevSkladby();
            if (Start.getStav() == 0 || Start.getStav() == 6) {
                tp.blit(frameBuffer, listaMenuInt, width - 240, 0, true);
            }
            text20.blitString(frameBuffer, jmenoInterpreta, x - text20.getStringBounds(jmenoInterpreta).width, y, 255, Color.blue);
            text20.blitString(frameBuffer, jmenoSkladby, x - text20.getStringBounds(jmenoSkladby).width, y + 26, 255, Color.yellow);

        }
    }

    private void menu() {
        int x = width - text50.getStringBounds("Nejlepší výsledky").width - 80;
        int y = height - text50.getStringBounds("Konec").height - 25;
        Color zvyrazneni = Color.yellow;
        Color neaktivni = Color.white;
        if (Start.getStav() == 0 && Menu.getI() == 1 && !Start.isScore()) {
            if (Start.inkrementZvyrazneniMenu == 0) {
                text50.blitString(frameBuffer, "Nová hra", x, y - 180, 255, zvyrazneni);
            } else {
                text50.blitString(frameBuffer, "Nová hra", x, y - 180, 255, neaktivni);
            }

            if (Start.inkrementZvyrazneniMenu == 1) {
                text50.blitString(frameBuffer, "Ovládání", x, y - 120, 255, zvyrazneni);
            } else {
                text50.blitString(frameBuffer, "Ovládání", x, y - 120, 255, neaktivni);
            }

            if (Start.inkrementZvyrazneniMenu == 2) {
                text50.blitString(frameBuffer, "Nejlepší výsledky", x, y - 60, 255, zvyrazneni);
            } else {
                text50.blitString(frameBuffer, "Nejlepší výsledky", x, y - 60, 255, neaktivni);
            }

            if (Start.inkrementZvyrazneniMenu == 3) {
                text50.blitString(frameBuffer, "Konec", x, y, 255, zvyrazneni);
            } else {
                text50.blitString(frameBuffer, "Konec", x, y, 255, neaktivni);
            }
        }

        if (Start.getStav() == 0 && Menu.getI() == 2 && !Start.isScore()) {
            text50.blitString(frameBuffer, "Jít hrát", width - 360, height - 70, 255, zvyrazneni);
        }

        if (Start.getStav() == 6) {
            text50.blitString(frameBuffer, "Pokračovat", width - 360, height - 70, 255, zvyrazneni);
        }

    }

    private void nabidkaVypnuti() {
        if (nabidkaV && !Postman.isZabit()) {
            tp.blit(frameBuffer, cernePozadiInt, 0, 0, width, height, 4, false, null);
            text50.blitString(frameBuffer, "Opravdu chcete skončit?", (width / 2) - 296, (height / 2) - 10, 255, Color.yellow);
            if (Start.inkrementZvyrazneniNabidek == 0) {
                text30.blitString(frameBuffer, "Ano", (width / 2) - 204, (height / 2) + 50, 255, Color.blue);
            } else {
                text30.blitString(frameBuffer, "Ano", (width / 2) - 204, (height / 2) + 50, 255, Color.white);
            }

            if (Start.inkrementZvyrazneniNabidek == 1) {
                text30.blitString(frameBuffer, "Ne", (width / 2) + 155, (height / 2) + 50, 255, Color.blue);
            } else {
                text30.blitString(frameBuffer, "Ne", (width / 2) + 155, (height / 2) + 50, 255, Color.white);
            }
        }
    }

    private void nabidkaMenu() {
        if (nabidkaM && !Postman.isZabit()) {
            int x = (width / 2) - 105;
            Color aktivni = Color.blue;
            tp.blit(frameBuffer, cernePozadiInt, 0, 0, width, height, 4, false, null);
            text80.blitString(frameBuffer, "Pauza", x - 9, (height / 2) - 90, 255, Color.yellow);
            if (Start.inkrementZvyrazneniNabidek == 0) {
                text50.blitString(frameBuffer, "Hrát dál", x + 6, (height / 2) - 30, 255, aktivni);
            } else {
                text50.blitString(frameBuffer, "Hrát dál", x + 6, (height / 2) - 30, 255, Color.white);
            }
            if (Start.inkrementZvyrazneniNabidek == 1) {
                text50.blitString(frameBuffer, "Restart kola", x - 44, (height / 2) + 30, 255, aktivni);
            } else {
                text50.blitString(frameBuffer, "Restart kola", x - 44, (height / 2) + 30, 255, Color.white);
            }
            if (Start.inkrementZvyrazneniNabidek == 2) {

                text50.blitString(frameBuffer, "Odchod do menu", x - 94, (height / 2) + 90, 255, aktivni);
            } else {
                text50.blitString(frameBuffer, "Odchod do menu", x - 94, (height / 2) + 90, 255, Color.white);
            }

            if (Start.inkrementZvyrazneniNabidek == 3) {
                text50.blitString(frameBuffer, "Konec hry", x - 16, (height / 2) + 150, 255, aktivni);
            } else {
                text50.blitString(frameBuffer, "Konec hry", x - 16, (height / 2) + 150, 255, Color.white);
            }
        }
    }

    private void zabitiPostaka() {
        if (Postman.isZabit()) {
            if (nabidkaM) {
                nabidkaM = false;
            }
            if (nabidkaV) {
                nabidkaV = false;
            }
            tp.blit(frameBuffer, cernePozadiInt, 0, 0, width, height, 255, false, null);
            text80.blitString(frameBuffer, "Byl jsi sežrán", (width / 2) - 248, (height / 2) - 20, 255, Color.yellow);
            text20.blitString(frameBuffer, "Pro pokračování stiskni mezerník nebo klikni myší", (width / 2) - 237, (height / 2) + 25, 255, Color.blue);
        }
    }

    private void fpsCounter() {
        if (blitFPS) {
            text30.blitString(frameBuffer, "FPS: " + pocitadloFps() + ", při malém počtu FPS zkuste vypnout mlhu.", 10, height - 10, 255, Color.white);
        }
    }

    private void priradNazevSkladby() {
        switch (Start.getStav()) {
            case 0:
                jmenoInterpreta = "PeerGynt Lobogris";
                jmenoSkladby = "Souls of Gaia";
                break;
            case 1:
                jmenoInterpreta = "Moby";
                jmenoSkladby = "Scream Pilots";
                break;
            case 2:
                jmenoInterpreta = "Various Artists";
                jmenoSkladby = "EHMA - Pizzicato";
                break;
            case 3:
                jmenoInterpreta = "Efiel";
                jmenoSkladby = "Countryside";
                break;
            case 4:
                jmenoInterpreta = "Silence is Sexy";
                jmenoSkladby = "1984";
                break;
            case 5:
                jmenoInterpreta = "Arnaud Condé";
                jmenoSkladby = "Thème d'Enoriel";
                break;
            case 6:
                jmenoInterpreta = "Silence is Sexy";
                jmenoSkladby = "Talk (Instrumental)";
                break;
            default:
                jmenoSkladby = "!NÁZEV SKLADBY NELZE NAČÍST!";
                System.out.println("Stav " + Start.getStav() + " - chyba - nelze načíst název skladby.");
        }
    }

    private void jmenoKola() {
        switch (Start.getStav()) {
            case 1:
                jmenoKola = "Pondělí";
                break;
            case 2:
                jmenoKola = "Úterý";
                break;
            case 3:
                jmenoKola = "Středa";
                break;
            case 4:
                jmenoKola = "Čtvrtek";
                break;
            case 5:
                jmenoKola = "Pátek";
                break;
            default:
                jmenoSkladby = "!NÁZEV NELZE NAČÍST!";
                System.out.println("Stav " + Start.getStav() + " - chyba - nelze načíst název kola.");
        }
    }

    private int pocitadloFps() {
        if (System.currentTimeMillis() - time >= 1000) {
            lfps = fps;
            fps = 0;
            time = System.currentTimeMillis();
        }
        fps++;
        return lfps;
    }

    public static boolean isNabidkaV() {
        return nabidkaV;
    }

    public static void setNabidkaV(boolean nabidkaV) {
        Blity.nabidkaV = nabidkaV;
    }

    public static boolean isNabidkaM() {
        return nabidkaM;
    }

    public static void setNabidkaM(boolean nabidkaM) {
        Blity.nabidkaM = nabidkaM;
    }

    public static boolean isBlitFPS() {
        return blitFPS;
    }

    public static void setBlitFPS(boolean blitFPS) {
        Blity.blitFPS = blitFPS;
    }
}
