/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core.utils;

/**
 *
 * @author tomas
 */
public class Timer {

    public static class Ticker{

        private final int rate;
        private long s2;

        public static long getTime() {
            return System.nanoTime();
        }

        public Ticker(int tickrateMS) {
            rate = tickrateMS;
            s2 = Ticker.getTime();
        }

        public int getTicks() {
            long i = Ticker.getTime();
            if (i - s2 > rate) {
                int ticks = (int) ((i - s2) / (long) rate);
                s2 += (long) rate * ticks;
                return ticks;
            }
            return 0;
        }
    }
}
