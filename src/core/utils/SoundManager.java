/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core.utils;

import paulscode.sound.SoundSystemJPCT;

/**
 *
 * @author tomas
 */
public class SoundManager {

//0 fanfara
//1 skok
//2 sežranej pošták
//3 sebrání dopisu    
//4 sebrání mince
//5 sebrání bonusu
    private static boolean ticho;
    private static SoundSystemJPCT sound;

    public SoundManager(boolean ticho) {
        SoundManager.ticho = ticho;
        sound = new SoundSystemJPCT();
        sound.setMasterVolume(0.95f);
    }

    public static void prehrajZvuk(int cislo) {
        try {
            if (!ticho) {
                if (cislo == 1) { //odstranění vícenásobného přehrávání zvuku při skoku pošťáka
                    if (!sound.playing()) {
                        sound.quickPlay("sound" + cislo + ".wav", false);
                    }
                } else {
                    sound.quickPlay("sound" + cislo + ".wav", false);
                }
            }

// Podmínku nelze napsat takto jednoduše, protože !sound.playing() zabraňuje přehrání více zvuků (například výskok a mince zasebou se přehraje pouze výskok)!!            
//            if (!ticho && !sound.playing()) {
//                sound.quickPlay("sound" + cislo + ".wav", false);
//            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void vycisti() {
        sound.cleanup();
    }
}
