/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core.utils;

import core.Start;
import java.io.IOException;
import org.newdawn.easyogg.OggClip;

/**
 *
 * @author tomas
 */
public class MusicManager {

    private final boolean ticho;
    private final byte pocetSkladeb = 7;
    private final OggClip music[] = new OggClip[pocetSkladeb];
    private byte cisloStavu;

    public MusicManager(boolean ticho) {
        this.ticho = ticho;
        if (!ticho) {
            nactiPlaylist();
        }
    }

    private void nactiPlaylist() {
        try {
            for (byte i = 0; i <= pocetSkladeb - 1; i++) {
                music[i] = new OggClip("Music/music" + i + ".ogg");
            }
        } catch (IOException e) {
            System.out.println("Hudební manažer - chyba: " + e);
        }
    }

    public void ziskejStav() { //detekuji, kde se hráč nachazí, podle toho pouštim hudbu
        if (!ticho) {
            cisloStavu = Start.getStav();
            for (byte i = 0; i <= pocetSkladeb - 1; i++) {
                if (cisloStavu == i && music[i].stopped()) {
                    hraj();
                }
            }
            
        }
    }

    private void hraj() {
        for (int i = cisloStavu + 1; i < pocetSkladeb; i++) {
            music[i].stop();
            System.out.println("Vypínám hudbu stavu " + i);
        }
        for (int i = cisloStavu - 1; i > -1; i--) {
            music[i].stop();
            System.out.println("Vypínám hudbu stavu " + i);
        }
        music[cisloStavu].setGain(0.85f);
        music[cisloStavu].loop();
        System.out.println("Pouštím hudbu pro stav " + cisloStavu);
    }

    public void stop() {
        music[cisloStavu].stop();
        music[cisloStavu].close();
    }
    
}
