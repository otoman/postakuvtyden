/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core.objects;

import com.threed.jpct.Animation;
import com.threed.jpct.CollisionEvent;
import com.threed.jpct.CollisionListener;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import core.Start;
import core.states.Game;
import core.utils.Load;
import core.utils.SoundManager;
import core.utils.Timer;
import org.lwjgl.input.Mouse;

/**
 *
 * @author tomas
 */
public class Postman extends Object3D {

    private static Object3D postakModel;
    private static boolean beh = false;
    private static boolean dozadu = false;
    private static boolean dopredu = false;
    private static boolean skok = false;
    private static boolean doleva = false;
    private static boolean doprava = false;
    private static SimpleVector moveRes = new SimpleVector(0, 0, 0);
    private static final SimpleVector ellipsoid = new SimpleVector(10, 34f, 8);
    private static final float SPEED = 2f;
    private static float MAXSPEED = 2f;
    private static final float DAMPING = 0.1f;
    private static final Timer.Ticker ticker = new Timer.Ticker(1500);
    private static long ticks = 0;
    private static int animace;
    private float index;
    private final float scale = 0.2f;
    private Animation anim;
    private boolean akceSkok;
    private int skokinkrement = -7;
    private static boolean zabit;
    private float inkrementIndexu;

    public Postman(Object3D postak) {
        super(postak);
        init();
        kolize();
    }

    private void init() {
        setCollisionMode(Object3D.COLLISION_CHECK_SELF | Object3D.COLLISION_CHECK_OTHERS);
        postakModel = Load.loadModel("assets/objects/postak.3ds", scale);

        anim = new Animation(5);
        anim.createSubSequence("chuze");
        anim.addKeyFrame(postakModel.getMesh());
        anim.addKeyFrame(Load.loadModel("assets/objects/postak_levanoha.3ds", scale).getMesh());
        anim.addKeyFrame(postakModel.getMesh()); // pro úsporu paměti používám už loadnutý model
        anim.addKeyFrame(Load.loadModel("assets/objects/postak_pravanoha.3ds", scale).getMesh());

        anim.createSubSequence("stuj");
        anim.addKeyFrame(postakModel.getMesh());

        setAnimationSequence(anim);
        zabit = false;
    }

    public void animace() {
        {
            index += inkrementIndexu;
            if (index > 1f) {
                index -= 1f;
            }
        }
        animate(index, animace);
    }

    public void update() {
        ticks = ticker.getTicks();

        if (ticks >= 0) {
            if (!Start.pauza) {
                move(ticks);
                animace();
            }

        }
    }

    private void move(long ticks) {
        if (ticks == 0) {
            return;
        }

        if (beh) {
            inkrementIndexu = 0.045f;
            SimpleVector t = this.getZAxis();
            MAXSPEED = 3.5f;
            t.scalarMul(-SPEED * 1.75f);
            moveRes.add(t);
            animace = 1;
        } else {
            MAXSPEED = 2f;
            inkrementIndexu = 0.025f;
        }

        if (dopredu) {
            inkrementIndexu = 0.03f;
            SimpleVector t = this.getZAxis();
            t.scalarMul(-SPEED);
            moveRes.add(t);
            animace = 1;
        } else {
            inkrementIndexu = 0.025f;
        }

        if (dozadu) {
            SimpleVector t = this.getZAxis();
            t.scalarMul(SPEED);
            moveRes.add(t);
            animace = 1;
        }

        this.rotateAxis(this.getYAxis(), -Mouse.getDX() / 500f);

        if (doleva) {
            SimpleVector t = this.getXAxis();
            t.scalarMul(SPEED / 2);
            moveRes.add(t);
            animace = 1;
        }

        if (doprava) {
            SimpleVector t = this.getXAxis();
            t.scalarMul(-SPEED / 2);
            moveRes.add(t);
            animace = 1;
        }

        if (skok && akceSkok && !(Postman.isZabit())) { //skok jen kdyz je kontakt s objektem
            // aplikace skoku:
            SoundManager.prehrajZvuk(1);
            SimpleVector s = new SimpleVector(0, skokinkrement, 0);
            s = this.checkForCollisionEllipsoid(s, ellipsoid, 3);
            this.translate(s);
            animace = 2;
            if (s.length() == skokinkrement) { //dotaz na kontakt se zemí
                akceSkok = false; //nelze skocit
            }
            skokinkrement += 0.1;
        } else {
            // aplikace gravitace:
            SimpleVector t = new SimpleVector(0, 2, 0);
            t = this.checkForCollisionEllipsoid(t, ellipsoid, 3);
            this.translate(t);
            skokinkrement = -7;
            if (t.length() == 2) { //dotaz na kontakt se zemí
                akceSkok = false; //nelze skocit
            } else {
                if (!skok) {
                    akceSkok = true;  //  lze skocit
                }
            }
        }

        // snizeni velke rychlosti
        if (moveRes.length() > MAXSPEED) {
            moveRes.makeEqualLength(new SimpleVector(0, 0, MAXSPEED));
        }

        moveRes = this.checkForCollisionEllipsoid(moveRes, ellipsoid, 3); //smer, polomer elipsoidu, pocet iteraci
        this.translate(moveRes);

        // brzdeni
        if (moveRes.length() > DAMPING) {
            moveRes.makeEqualLength(new SimpleVector(0, 0, DAMPING));
        } else {
            moveRes = new SimpleVector(0, 0, 0);
        }
    }

    private void kolize() {
        addCollisionListener(new CollisionListener() {

            @Override
            public void collision(CollisionEvent ce) {
                if (ce.getType() == CollisionEvent.TYPE_SOURCE && ce.getSource() != null) {
                    //sběr dopisů
                    Object3D[] cilDopisy = ce.getTargets();
                    for (Object3D objekt : cilDopisy) {
                        if (objekt instanceof Letter) {
                            SoundManager.prehrajZvuk(3);
                            Letter dopis = (Letter) objekt;
                            dopis.setCollisionMode(Object3D.COLLISION_CHECK_NONE);
                            dopis.disableCollisionListeners();
                            dopis.setVisibility(false);
                            Game.pocetDopisu++;
                        }
                    }

                    //sběr mincí
                    Object3D[] cilMince = ce.getTargets();
                    for (Object3D objekt : cilMince) {
                        if (objekt instanceof Coin) {
                            SoundManager.prehrajZvuk(4);
                            Coin mince = (Coin) objekt;
                            mince.setCollisionMode(Object3D.COLLISION_CHECK_NONE);
                            mince.disableCollisionListeners();
                            mince.setVisibility(false);
                            Game.pocetBodu += 10;
                        }
                    }

                    //smrt pošťáka od Požírače dopisového
                    Object3D[] cilDopisovac = ce.getTargets();
                    for (Object3D objekt : cilDopisovac) {
                        if (objekt instanceof PoziracDopisovy) {
                            SoundManager.prehrajZvuk(2);
                            Game.pocetBodu -= 10;
                            setCollisionMode(0);
                            zabit = true;
                        }
                    }

                    //smrt pošťáka od Požírače balíkového                    
                    Object3D[] cilBalikovac = ce.getTargets();
                    for (Object3D objekt : cilBalikovac) {
                        if (objekt instanceof PoziracBalikovy) {
                            SoundManager.prehrajZvuk(2);
                            Game.pocetBodu -= 10;
                            setCollisionMode(0);
                            zabit = true;
                        }
                    }

                    //cíl
                    Object3D[] cilCil = ce.getTargets();
                    for (Object3D objekt : cilCil) {
                        if (objekt instanceof Finish) {
                            SoundManager.prehrajZvuk(0);
                            Game.pocetBodu += 20;
                            Game.setvCili(true);
                        }
                    }
                }

            }

            @Override
            public boolean requiresPolygonIDs() {
                return false;
            }
        }
        );
    }

    public static boolean isZabit() {
        return Postman.zabit;
    }

    public static void setZabit(boolean zabit) {
        Postman.zabit = zabit;
    }

    public static void setAnimace(int animace) {
        Postman.animace = animace;
    }

    public static void setDozadu(boolean dozadu) {
        Postman.dozadu = dozadu;
    }

    public static void setDopredu(boolean dopredu) {
        Postman.dopredu = dopredu;
    }

    public static void setDoleva(boolean doleva) {
        Postman.doleva = doleva;
    }

    public static void setDoprava(boolean doprava) {
        Postman.doprava = doprava;
    }

    public static void setSkok(boolean skok) {
        Postman.skok = skok;
    }

    public static void setBeh(boolean beh) {
        Postman.beh = beh;
    }
}
