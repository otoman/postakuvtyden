/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core.objects;

import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import core.utils.Timer;

/**
 *
 * @author tomas
 */
public class Finish extends Object3D {

    private final Timer.Ticker ticker = new Timer.Ticker(1500);
    long ticks = 0;
    private final SimpleVector ellipsoid = new SimpleVector(2, 20, 2);

    public Finish(Object3D cil) {
        super(cil);
        setCollisionMode(Object3D.COLLISION_CHECK_SELF | Object3D.COLLISION_CHECK_OTHERS);
    }

    public void update() {
        ticks = ticker.getTicks();
        if (ticks >= 0) {
            move(ticks);
        }
    }

    private void move(long ticks) {

        if (ticks == 0) {
            return;
        }

        //gravitace
        SimpleVector t = new SimpleVector(0, 1, 0);
        t = this.checkForCollisionEllipsoid(t, ellipsoid, 1);
        this.translate(t);
    }
}
