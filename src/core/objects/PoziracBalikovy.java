/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core.objects;

import com.threed.jpct.CollisionEvent;
import com.threed.jpct.CollisionListener;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import core.Start;
import core.states.Game;
import core.utils.SoundManager;
import core.utils.Timer;

/**
 *
 * @author tomas
 */
public class PoziracBalikovy extends Object3D {

    private final Timer.Ticker ticker = new Timer.Ticker(1500);
    long ticks = 0;
    private SimpleVector moveRes = new SimpleVector(0, 0, 0);
    private final SimpleVector ellipsoid = new SimpleVector(20, 12.2, 20);
    private final float DAMPING = 0.1f;
    private final float MAXSPEED = 1f;
    private SimpleVector chuze;
    private final float RYCHLOST = 2;

    public PoziracBalikovy(Object3D poziracBalikovy) {
        super(poziracBalikovy);
        setCollisionMode(Object3D.COLLISION_CHECK_SELF | Object3D.COLLISION_CHECK_OTHERS);
        kolize();
    }

    public void update() {
        ticks = ticker.getTicks();
        if (ticks >= 0) {
            move(ticks);
        }
    }

    private void move(long ticks) {

        if (ticks == 0) {
            return;
        }

        //rychlost
        if (moveRes.length() > MAXSPEED) {
            moveRes.makeEqualLength(new SimpleVector(0, 0, MAXSPEED));
        }

        chuze = this.getZAxis();
        chuze.scalarMul(RYCHLOST);
        moveRes.add(chuze);

        moveRes = this.checkForCollisionEllipsoid(moveRes, ellipsoid, 1);

        if (!Start.pauza) {
            rotace(moveRes);
            this.translate(moveRes);
        }

        //gravitace
        SimpleVector t = new SimpleVector(0, 2, 0);
        t = this.checkForCollisionEllipsoid(t, ellipsoid, 1);
        this.translate(t);

        if (moveRes.length() > DAMPING) {
            moveRes.makeEqualLength(new SimpleVector(0, 0, DAMPING));
        } else {
            moveRes = new SimpleVector(0, 0, 0);
        }
    }

    public void rotace(SimpleVector smer) {
        if (smer.length() < RYCHLOST) {
            if (Start.getStav() == 3 || Start.getStav() == 5) {
                rotateY(otoceniCelyUhel());
            } else {
                rotateY((float) (Math.random() * Math.PI));
            }
        }
    }

    private float otoceniCelyUhel() {
        int i = (int) (Math.random() * 2);
        float uhel;
        switch (i) {
            case 1:
                uhel = (float) Math.PI / 2;
                break;
            case 2:
                uhel = (float) -Math.PI / 2;
                break;
            default:
                uhel = (float) Math.PI;
                break;
        }
        return uhel;
    }

    private void kolize() {
        addCollisionListener(new CollisionListener() {

            @Override
            public void collision(CollisionEvent ce) {
                if (ce.getType() == CollisionEvent.TYPE_SOURCE && ce.getSource() != null) {
                    //smrt pošťáka od Požírače balíkového
                    Object3D[] cilPostak = ce.getTargets();
                    for (Object3D objekt : cilPostak) {
                        if (objekt instanceof Postman) {
                            SoundManager.prehrajZvuk(2);
                            Game.pocetBodu -= 10;
                            setCollisionMode(0);
                            Postman.setZabit(true);
                        }
                    }
                }
            }

            @Override
            public boolean requiresPolygonIDs() {
                return false;
            }
        }
        );
    }

}
