/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core.objects;

import com.threed.jpct.CollisionEvent;
import com.threed.jpct.CollisionListener;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import core.Start;
import core.states.Game;
import core.utils.SoundManager;
import core.utils.Timer;

/**
 *
 * @author tomas
 */
public class Letter extends Object3D {

    private final Timer.Ticker ticker = new Timer.Ticker(1500);
    long ticks = 0;
    private final SimpleVector ellipsoid = new SimpleVector(2, 15, 2);

    public Letter(Object3D dopis) {
        super(dopis);
        setCollisionMode(Object3D.COLLISION_CHECK_SELF | Object3D.COLLISION_CHECK_OTHERS);
        kolize();
    }

    public void update() {
        ticks = ticker.getTicks();
        if (ticks >= 0) {
            move(ticks);
        }
        if (!Start.pauza) {
            this.rotateY(0.1f);
        }
    }

    private void move(long ticks) {

        if (ticks == 0) {
            return;
        }

        //gravitace
        SimpleVector t = new SimpleVector(0, 2, 0);
        t = this.checkForCollisionEllipsoid(t, ellipsoid, 1);
        this.translate(t);
    }

    private void kolize() {
        addCollisionListener(new CollisionListener() {

            @Override
            public void collision(CollisionEvent ce) {
                if (ce.getType() == CollisionEvent.TYPE_SOURCE && ce.getSource() != null) {
                    //sběr dopisu - zdrojem dopis
                    Object3D[] cilPostak = ce.getTargets();
                    for (Object3D objekt : cilPostak) {
                        if (objekt instanceof Postman) {
                            if (!Postman.isZabit()) {
                                SoundManager.prehrajZvuk(3);
                            }
                            setCollisionMode(Object3D.COLLISION_CHECK_NONE);
                            disableCollisionListeners();
                            setVisibility(false);
                            Game.pocetDopisu++;
                        }
                    }
                }
            }

            @Override
            public boolean requiresPolygonIDs() {
                return false;
            }
        }
        );
    }
}
