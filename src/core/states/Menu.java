/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core.states;

import com.threed.jpct.Camera;
import com.threed.jpct.Object3D;
import com.threed.jpct.Primitives;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;
import com.threed.jpct.util.Light;
import core.Start;

/**
 *
 * @author tomas
 */
public class Menu extends World implements States {

    private Light light;
    private Object3D plochaProPozadi;
    private Camera cam;
    private static int i;

    public Menu() {
        i = 1;
        init();
    }

    private void init() {
        loadTexture();

        Start.setStav((byte)0);

        //plochaProPozadi
        plochaProPozadi = Primitives.getPlane(20, 15);
        plochaProPozadi.rotateX((float) Math.PI / 1f);
        plochaProPozadi.rotateY((float) Math.PI / 1f);
        plochaProPozadi.rotateZ((float) Math.PI / 2f);
        plochaProPozadi.translate(0, 0, 100);
        plochaProPozadi.rotateZ(1.57f);
        plochaProPozadi.scale(0.9f);
        plochaProPozadi.setTexture("MenuPozadi1");

        //kamera
        cam = this.getCamera();
        cam.setPosition(0, 0, -100);
        cam.lookAt(plochaProPozadi.getTransformedCenter());

        //světlo
        light = new Light(this);
        light.setPosition(new SimpleVector(0, -130, 0));
        light.setIntensity(30, 30, 30);
        light.setAttenuation(-1);

        this.addObjects(new Object3D[]{
            plochaProPozadi
        });
        this.buildAllObjects();
    }

    @Override
    public void update() {
        plochaProPozadi.setTexture("MenuPozadi" + i);
    }

    @Override
    public void dispose() {
        super.dispose();
        removeAll();
        TextureManager.getInstance().flush();
    }

    @Override
    public void loadTexture() {
        TextureManager.getInstance().addTexture("MenuPozadi1", new Texture("assets/textures/menu_bg.png"));
        TextureManager.getInstance().addTexture("MenuPozadi2", new Texture("assets/textures/story_bg.png"));
        TextureManager.getInstance().addTexture("MenuPozadi3", new Texture("assets/textures/ovladani_bg.png"));
    }

    public static void setI(int i) {
        Menu.i = i;
    }

    public static int getI() {
        return i;
    }
    
}
