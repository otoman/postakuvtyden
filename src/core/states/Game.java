/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core.states;

import com.threed.jpct.Camera;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;
import com.threed.jpct.util.Light;
import core.Start;
import core.objects.Finish;
import core.objects.Letter;
import core.objects.Coin;
import core.objects.PoziracDopisovy;
import core.objects.Postman;
import core.objects.PoziracBalikovy;
import core.utils.LevelManager;
import core.utils.Load;
import java.util.logging.Level;
import org.lwjgl.input.Mouse;

/**
 *
 * @author tomas
 */
public class Game extends World implements States {

    private static Camera cam;
    private static Light light;
    private static boolean dalsiKolo;
    private static Postman postak;
    private LevelManager levelManager;
    private static final int pocetKol = 5;
    private static byte level;
    private PoziracDopisovy[] poziraciDopisovi;
    private PoziracBalikovy[] poziraciBalikovi;
    private static Letter[] dopisy;
    private Coin[] mince;
    private Finish[] cil;
    private static boolean vCili;
    private final float scale = 0.2f;
    private Object3D postakModel;
    public static int pocetDopisu;
    public static int pocetBodu;
    private static boolean priblizit;
    private static boolean oddalit;
    private static boolean vychoziPozice;
    private float vzdalenostKamery = 120;
    private float nahoruDolu = 0.1f;

    public Game() {
        level = 1;
        dalsiKolo = false;
        pocetDopisu = 0;
        pocetBodu = 0;
        Postman.setDopredu(false);
        Postman.setBeh(false);
        Postman.setDoleva(false);
        Postman.setDoprava(false);
        Postman.setDozadu(false);
        Postman.setSkok(false);
        Postman.setAnimace(2);
        loadSceny(level);
        vCili = false;
        Start.setStav((byte) 1);
    }

    private void loadSceny(byte cislo) {
        init();
        levelManager = new LevelManager(cislo);

        levelManager.mapa.setTexture("map" + levelManager.cisloKola);
        levelManager.mapa.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
        levelManager.mapa.build();
        addObject(levelManager.mapa);

        postakModel = Load.loadModel("assets/objects/postak.3ds", scale);
        postak = new Postman(postakModel);
        TextureManager.getInstance().addTexture("texturaPostaka", new Texture("assets/objects/postak.png"));
        postak.setTexture("texturaPostaka");
        postak.translate(0, -100, 0);
        postak.build();
        addObject(postak);

        poziraciDopisovi = new PoziracDopisovy[levelManager.pocetPD];
        for (int i = 0; i < poziraciDopisovi.length; i++) {
            poziraciDopisovi[i] = new PoziracDopisovy(levelManager.poziracDopisovyPole.get("poziracDopisovy" + i));
            poziraciDopisovi[i].build();
            addObject(poziraciDopisovi[i]);
        }

        poziraciBalikovi = new PoziracBalikovy[levelManager.pocetPB];
        for (int i = 0; i < poziraciBalikovi.length; i++) {
            poziraciBalikovi[i] = new PoziracBalikovy(levelManager.poziracBalikovyPole.get("poziracBalikovy" + i));
            poziraciBalikovi[i].build();
            addObject(poziraciBalikovi[i]);
        }

        //Na objekty nepůsobí gravitace => není potřeba vytvářet zvláštní třídu
        Object3D[] bedny = new Object3D[levelManager.pocetB];
        for (int i = 0; i < bedny.length; i++) {
            bedny[i] = levelManager.bednaPole.get("bedna" + i);
            bedny[i].setCollisionMode(Object3D.COLLISION_CHECK_SELF | Object3D.COLLISION_CHECK_OTHERS);
            bedny[i].build();
            addObject(bedny[i]);
        }

        Object3D[] cihly = new Object3D[levelManager.pocetC];
        for (int i = 0; i < cihly.length; i++) {
            cihly[i] = levelManager.cihlaPole.get("cihla" + i);
            cihly[i].setCollisionMode(Object3D.COLLISION_CHECK_SELF | Object3D.COLLISION_CHECK_OTHERS);
            cihly[i].build();
            addObject(cihly[i]);
        }

        dopisy = new Letter[levelManager.pocetD];
        for (int i = 0; i < dopisy.length; i++) {
            dopisy[i] = new Letter(levelManager.dopisPole.get("dopis" + i));
            dopisy[i].build();
            addObject(dopisy[i]);
        }

        mince = new Coin[levelManager.pocetM];
        for (int i = 0; i < mince.length; i++) {
            mince[i] = new Coin(levelManager.mincePole.get("mince" + i));
            mince[i].build();
            addObject(mince[i]);
        }

        cil = new Finish[levelManager.pocetCilu];
        for (int i = 0; i < cil.length; i++) {
            cil[i] = new Finish(levelManager.cilPole.get("cil" + i));
            cil[i].build();
            addObject(cil[i]);
            cil[i].setVisibility(false);
        }
    }

    private void init() {
        cam = this.getCamera(); // kamera
        cam.setPosition(300, -60, 0); // počáteční pozice kamery

        light = new Light(this); // světlo
        light.setPosition(new SimpleVector(0, -80, 0));
        light.setIntensity(140, 120, 120);
        light.setAttenuation(-1);

        Postman.setDopredu(false);
        Postman.setDoleva(false);
        Postman.setDoprava(false);
        Postman.setDozadu(false);
        Postman.setSkok(false);
        Postman.setAnimace(2);

        if (Start.isPovolitMlhu()) {
            setFogging(World.FOGGING_ENABLED);
            setFoggingMode(World.FOGGING_PER_PIXEL);
            setFogParameters(430, 500, 171, 233, 233);
        }
    }

    @Override
    public void update() {
        moveCamera();
        moveLevel();

        boolean klikLibovolnym = false;

        while (Mouse.next()) {
            if (Mouse.getEventButtonState()) {
                klikLibovolnym = true;
            } else {
                if (Mouse.getEventButton() == 2 && !Postman.isZabit()) {
                    vzdalenostKamery = 120f;
                }
            }
        }

        if (Start.restart || (Postman.isZabit() && klikLibovolnym)) {
            restartLevel();
        }
        try {
            if (postak.getVisibility()) {
                postak.update();
            }
            for (PoziracDopisovy poziracDopisovy : poziraciDopisovi) {
                if (poziracDopisovy.getVisibility()) {
                    poziracDopisovy.update();
                }
            }
            for (PoziracBalikovy poziracBalikovy : poziraciBalikovi) {
                if (poziracBalikovy.getVisibility()) {
                    poziracBalikovy.update();
                }
            }
            for (Letter dopis : dopisy) {
                if (dopis.getVisibility()) {
                    dopis.update();
                }
            }
            for (Coin minceM : mince) {
                if (minceM.getVisibility()) {
                    minceM.update();
                }
            }

            for (Finish cilC : cil) {
                if (cilC.getVisibility()) {
                    cilC.update();
                }
                if (pocetDopisu == dopisy.length) {
                    cilC.setVisibility(true);
                }
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void moveCamera() {

        if (!Start.pauza) {
            if (priblizit) {
                vzdalenostKamery -= 10;
            }

            if (oddalit) {
                vzdalenostKamery += 10;
            }

            if (vychoziPozice) {
                vzdalenostKamery = 120f;
            }

            if (vzdalenostKamery < 69) {
                vzdalenostKamery = 69;
            }
            if (vzdalenostKamery > 200) {
                vzdalenostKamery = 200;
            }

            vzdalenostKamery += Mouse.getDWheel() * 0.05;

        }
//        if (vzdalenostKamery < 70) {
//            prvniPozice();
//        } else {
        tretiPozice();
//        }

    }

    private void prvniPozice() {
        int y = Mouse.getY();
        nahoruDolu = y / 1000f;
        if (nahoruDolu > 0.103f) {
            nahoruDolu = 0.103f;
        }
        if (nahoruDolu < 0.095) {
            nahoruDolu = 0.095f;
        }

        SimpleVector oldCamPos = cam.getPosition();
        oldCamPos.scalarMul(9f);
        SimpleVector postakCenter = postak.getTransformedCenter();
        SimpleVector zOffset = postak.getZAxis();
        zOffset.scalarMul(-150);
        SimpleVector camPos = new SimpleVector(postakCenter);
        postakCenter.add(new SimpleVector(0, -14, 0));
        camPos.add(zOffset);
        camPos.add(oldCamPos);
        camPos.scalarMul(nahoruDolu);
        cam.setPosition(postakCenter);
        cam.lookAt(camPos);
    }

    private void tretiPozice() {
        SimpleVector oldCamPos = cam.getPosition();
        oldCamPos.scalarMul(9f);
        SimpleVector postakCenter = postak.getTransformedCenter();
        SimpleVector zOffset = postak.getZAxis();
        zOffset.scalarMul(vzdalenostKamery);
        SimpleVector camPos = new SimpleVector(postakCenter);
        camPos.add(new SimpleVector(0, -25, 0));
        camPos.add(zOffset);
        camPos.add(oldCamPos);
        camPos.scalarMul(0.1f);
        cam.setPosition(camPos);
        cam.lookAt(postakCenter);
    }

    @Override
    public void loadTexture() {
    }

    private void moveLevel() {
        if (dalsiKolo || vCili) {
            if (pocetKol > level) {
                level++;
                vCili = false;
                pocetDopisu = 0;
                dispose();
                levelManager.vycistiKolo();
                loadSceny(level);
                Start.setStav(level);
            } else {

                postak.setVisibility(false);
                for (PoziracDopisovy poziraciDopisoviP : poziraciDopisovi) {
                    poziraciDopisoviP.setVisibility(false);
                }
                for (PoziracBalikovy poziraciBalikoviB : poziraciBalikovi) {
                    poziraciBalikoviB.setVisibility(false);
                }
                for (Letter dopisyD : dopisy) {
                    dopisyD.setVisibility(false);
                }
                for (Coin minceM : mince) {
                    minceM.setVisibility(false);
                }
                for (Finish cil1 : cil) {
                    cil1.setVisibility(false);
                }
                levelManager.vycistiKolo();
                dispose();
                Start.scoreManager.zapis();
                Start.konec();
            }
        }
    }

    public void restartLevel() {
        Start.restart = false;
        pocetDopisu = 0;
        dispose();
        levelManager.vycistiKolo();
        loadSceny(level);
        Start.setStav(level);
    }

    @Override
    public void dispose() {
        super.dispose();
        removeAll();
        TextureManager.getInstance().flush();
    }

    public static Letter[] getDopisy() {
        return dopisy;
    }

    public static void setDalsiKolo(boolean dalsiKolo) {
        Game.dalsiKolo = dalsiKolo;
    }

    public static void setvCili(boolean vCili) {
        Game.vCili = vCili;
    }

    public static int getPocetKol() {
        return pocetKol;
    }

    public static int getLevel() {
        return level;
    }

    public static void setPriblizit(boolean priblizit) {
        Game.priblizit = priblizit;
    }

    public static void setOddalit(boolean oddalit) {
        Game.oddalit = oddalit;
    }

    public static void setVychoziPozice(boolean vychoziPozice) {
        Game.vychoziPozice = vychoziPozice;
    }
}
